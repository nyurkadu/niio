from datetime import datetime
import inspect
import openpyxl


# This function save screen shot in case of test failure
# where the file name includes time stamp and the test name
def screenCapture(testname, driver):
    path = "c:\\temp\\"+ timeToString() + testname + ".png"
    driver.get_screenshot_as_file(path)


# This function returns the current function name
def whoami():
    frame = inspect.currentframe()
    return inspect.getouterframes(frame)[1].function


def getRowCount(file, sheetname):
    workbook = openpyxl.load_workbook(file)
    sheet = workbook.get_sheet_by_name(sheetname)
    return sheet.max_row


def getColumnCount(file, sheetname):
    workbook = openpyxl.load_workbook(file)
    sheet = workbook.get_sheet_by_name(sheetname)
    return sheet.max_columns


def readData(file, sheetname, rownum, columnnum):
    workbook = openpyxl.load_workbook(file)
    sheet = workbook.get_sheet_by_name(sheetname)
    return sheet.cell(row=rownum, column=columnnum).value


def writedData(file, sheetname, rownum, columnnum, data):
    workbook = openpyxl.load_workbook(file)
    sheet = workbook.get_sheet_by_name(sheetname)
    sheet.cell(row=rownum, column=columnnum).value = data
    workbook.save(file)


def timeToString():
    return str(datetime.now()).replace(":", "-", 2)