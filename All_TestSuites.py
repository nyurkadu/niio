import os
import sys
import unittest
from base import BaseFile
import HtmlTestRunner

from package1.TC_CorrectSignUp import LoginTest
from package1.TC_IncorrectUser import InvalidUserName
from package2.TC_EmptyFields import EmptyFields
from package2.TC_IncorrectPassword import InvalidPassword
from package3.TC_NavigationBar import NavigationBar

tc1=unittest.TestLoader().loadTestsFromTestCase(LoginTest)
tc2=unittest.TestLoader().loadTestsFromTestCase(InvalidUserName)
tc3=unittest.TestLoader().loadTestsFromTestCase(InvalidPassword)
tc4=unittest.TestLoader().loadTestsFromTestCase(EmptyFields)
tc5=unittest.TestLoader().loadTestsFromTestCase(NavigationBar)

sanityTestSuite=unittest.TestSuite([tc1])
functionalTestSuite=unittest.TestSuite([tc3,tc4])
masterTestSuite=unittest.TestSuite([tc1,tc2,tc3,tc4,tc5])

current_directory = os.getcwd()
output_file = open(current_directory + "\HTML_Test_Runner_ReportTest.html", "w")

html_runner = HtmlTestRunner.HTMLTestRunner(
    stream=output_file,
    combine_reports=True,
    report_title='HTML Reporting using PyUnit',
    descriptions='HTML Reporting using PyUnit & HTMLTestRunner'
)
# html_runner.STYLESHEET_TMPL = '<link rel="stylesheet" href="my_stylesheet.css" type="text/css">'
if BaseFile.testtype == "masterTestSuite":
    results = unittest.TextTestRunner(verbosity=9).run(masterTestSuite)
    # unittest.TestRunner.run(masterTestSuite)
    print("results: %s" % results)
    print("results.wasSuccessful: %s" % results.wasSuccessful())

    if results.wasSuccessful():
        print("uraaa")
    else:
        print("ploho")
sys.exit(not results.wasSuccessful())
#     html_runner.run(masterTestSuite)



#
# if __name__=="__main__":
    # unittest.main( html_runner.run(masterTestSuite))
    # results = unittest.main( html_runner.run(masterTestSuite))
    # unittest.main( html_runner.run(masterTestSuite))
    # #unittest.main()
    # if BaseFile.testtype == "masterTestSuite":
    #     results = unittest.TestRunner(verbosity=9).run(masterTestSuite)
    # x = html_runner.combine_reports
    # print (x)

        # unittest.TestRunner(verbosity=9).run(masterTestSuite)
    # print("results: %s" % results)
    # print("results.wasSuccessful: %s" % results.wasSuccessful())
    #
    # if results.wasSuccessful():
    #     print("uraaa")
    # else:
    #     print("ploho")
    # sys.exit(not results.wasSuccessful())
