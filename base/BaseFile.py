import unittest
from selenium import webdriver
from utils import utils
import HtmlTestRunner


class BaseClass(unittest.TestCase):

    @classmethod
    def setUp(self):
        if browser == "Chrome":
            self.driver = webdriver.Chrome()
        elif browser == "FireFox":
            self.driver = webdriver.Firefox()
        elif browser == "IE":
            self.driver = webdriver.Ie()
        elif browser == "Edge":
            self.driver = webdriver.Edge()

        self.driver.maximize_window()
        print(testsite)
        self.driver.get(testsite)
        self.driver.implicitly_wait(20)

    @classmethod
    def tearDown(self):
        self.driver.quit()


browser = utils.readData(r"C:\Users\Avraham Lavi\PycharmProjects\niio\DataDrivenExample.xlsx", "Sheet1", 2, 1)
testtype = utils.readData(r"C:\Users\Avraham Lavi\PycharmProjects\niio\DataDrivenExample.xlsx", "Sheet1", 2, 2)
testsite = utils.readData(r"C:\Users\Avraham Lavi\PycharmProjects\niio\DataDrivenExample.xlsx", "Sheet1", 2, 3)


