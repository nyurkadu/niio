from selenium import webdriver
import time
from utils.utils import screenCapture
from utils.utils import whoami
from base.BaseFile import BaseClass
import logging


class InvalidPassword(BaseClass):

    def test_InvalidPassword(self):
        try:
            self.driver = webdriver.Chrome()
            self.driver.get("https://pre.niio.com/signin")

            self.driver.find_element_by_name("email").send_keys("guest01@niio.com")
            self.driver.find_element_by_name("password").send_keys("QaTes")
            self.driver.find_element_by_id("loginButton").click()

            time.sleep(5)

            self.assertTrue((self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div[1]/h3")).is_displayed())
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div[3]/button").click()
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)

