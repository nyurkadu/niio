from utils.utils import screenCapture
from utils.utils import whoami
from base.BaseFile import BaseClass
import logging
import time


#   This class created in addition to the original requirements because:
#  1 - when the one of two fields is empty the system react in different way
#  2 - demonstrate test which isn't pass
class EmptyFields(BaseClass):

    def test_EmptyFields(self):
        try:
            self.driver.find_element_by_name("email")
            self.driver.find_element_by_name("password")
            self.driver.find_element_by_id("loginButton").click()
            # time.sleep(30)

            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div[1]/div/div/div[1]/span").text
            self.assertTrue(elem == "Enter a valid email.")
            print (elem)

            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div[1]/div/div/div[2]/span").text
            print (elem)
            self.assertTrue(elem == "Required field")

            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div[2]/div").text
            print (elem)
            self.assertEqual( "Please complete the required fields.", elem , elem) #lease complete the required fields.

        except AssertionError as error:
            logging.info(error)
            testname = whoami()
            screenCapture(testname, self.driver)
            raise error

    def test_AllElementsExists(self):
        try:
            time.sleep(20)
            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div/div/div/a").text
            self.assertTrue(elem == "Forgot password?")
            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div[1]/div/div/span").text
            print(elem)
            self.assertTrue(elem == "Don't have an account? Join Us")
            elem = self.driver.find_element_by_xpath(
                "//*[@id='home-page']/div[1]/div/div[2]/ng-include/div/div[1]/form/div/div/div/span/a").text
            self.assertTrue(elem == "Join Us")

        #  here we can add more elements from the login page

        #  example how to scroll down to element which isnt displayed
            footer = self.driver.find_element_by_xpath("//*[@id='ng-app']/body/footer")
            self.driver.execute_script("arguments[0].scrollIntoView();", footer)
            elem = self.driver.find_element_by_xpath(
                "//*[@id='ng-app']/body/footer/div/div[1]/p/span[1]/a[1]").text
            self.assertTrue(elem == "Terms")
        except AssertionError as error:
            logging.info(error)
            testname = whoami()
            screenCapture(testname, self.driver)
            raise error


