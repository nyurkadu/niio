import time
from utils.utils import screenCapture
from utils.utils import whoami
from base.BaseFile import BaseClass
import logging


class NavigationBar(BaseClass):
    def test_RequestInvite(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[3]/nav/div/div/div/ul/li[1]/a").click()
            time.sleep(10)
            elem = self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/div/h3").text
            print(elem)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/form/div[2]/button[1]").click()
            self.assertTrue("Request an invite",elem)
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)

    def test_Join(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[3]/nav/div/div/div/ul/li[2]/a").click()
            time.sleep(10)
            elem = self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/div[1]/div[1]").text
            print(elem)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/div[1]/div[3]/div/button[1]").click()
            self.assertTrue("Join", elem)
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)

    def test_Login(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[3]/nav/div/div/div/ul/li[3]/a").click()
            time.sleep(10)
            elem = self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/div[1]/div/h3").text
            print(elem)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div/div/div/div[1]/div/button/span[1]").click()
            self.assertTrue("Login", elem)
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)

    def test_Home(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[3]/nav/div/div/a/ng-include").click()
            time.sleep(10)
            print(self.driver.current_url)
            self.assertTrue("https://pre.niio.com/site/", self.driver.current_url)

            elem = self.driver.find_element_by_xpath("//*[@id='menu-item-22392']/a/span").text
            print(elem)
            self.assertEqual("Home", elem, "Navigate to Home page ")

            elem = self.driver.find_element_by_xpath("//*[@id='menu-item-27458']").text
            print(elem)
            self.assertTrue("Art", elem)

            elem = self.driver.find_element_by_xpath("//*[@id='menu-item-24439']").text
            print(elem)
            self.assertTrue("Plans", elem)

            elem = self.driver.find_element_by_xpath("//*[@id='menu-item-23356']").text
            print(elem)
            self.assertTrue("Pro Tool ", elem)
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)