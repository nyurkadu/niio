from base.BaseFile import BaseClass
import time
import logging
from utils.utils import whoami, screenCapture


class LoginTest(BaseClass):

      def test_correctLoginByEmail(self):
        try:
            self.driver.find_element_by_name("email").send_keys("guest01@niio.com")
            self.driver.find_element_by_name("password").send_keys("QaTest01")
            self.driver.find_element_by_id("loginButton").click()
            time.sleep(5)
            self.assertTrue(self.driver.current_url == "https://pre.niio.com/home/portfolio/0/all")
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)




