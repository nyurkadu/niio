import time
from base.BaseFile import BaseClass
import logging
from utils.utils import whoami, screenCapture


class InvalidUserName(BaseClass):
    def test_InvalidUserName(self):
        try:
            self.driver.find_element_by_name("email").send_keys("aaaa@aaa.com")
            self.driver.find_element_by_name("password").send_keys("QaTest01")
            self.driver.find_element_by_id("loginButton").click()

            time.sleep(5)
            self.assertTrue((self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div[1]/h3")).is_displayed())
            self.driver.find_element_by_xpath("//*[@id='ng-app']/body/div[1]/div/div/div[3]/button").click()
        except Exception as e:
            logging.info(e)
            testname = whoami()
            screenCapture(testname, self.driver)


